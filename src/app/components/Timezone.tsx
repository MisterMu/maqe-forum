"use client"

function Timezone() {
  const tz = Intl.DateTimeFormat().resolvedOptions().timeZone
  return <span>{tz}</span>
}

export default Timezone
