import postService from "@/service/post.service"
import ForumCard from "./ForumCard"

async function ForumList() {
  const postList = await postService.getPostWithAuthor()

  return (
    <ul>
      {postList.map((p, index) => (
        <ForumCard
          key={p.id}
          post={p.post}
          author={p.author}
          isBlue={index % 2 === 1}
        />
      ))}
    </ul>
  )
}

export default ForumList
