import { TAuthor, TPost } from "@/service/post.service"
import dayjs from "dayjs"

import PostImage from "./PostImage"
import Author from "./Author"

type TProps = {
  author?: TAuthor
  post: TPost
  isBlue?: boolean
}

function ForumCard(props: TProps) {
  const { author, post, isBlue } = props

  const formattedDate = dayjs(post.created_at).format(
    "dddd, MMMM DD, YYYY, HH:mm"
  )

  return (
    <li
      className="rounded mb-6"
      style={{
        boxShadow: "0px 2px 4px 0px rgba(0, 0, 0, 0.15)",
        background: isBlue ? "#CEE6F8" : "white",
      }}
    >
      <div
        className="flex flex-row items-center border-b gap-1 py-2 px-4 text-sm font-medium text-gray-400"
        style={{
          borderColor: isBlue ? "rgba(0, 0, 0, 0.05)" : "rgba(0, 0, 0, 0.05)",
        }}
      >
        <Author author={author} />
        <span>posted on</span>
        <span>{formattedDate}</span>
      </div>

      <div className="flex md:flex-row flex-col gap-4 p-4">
        <div
          className="shrink-0 self-center md:self-start"
          style={{ width: 240, height: 180 }}
        >
          <PostImage id={post.id} />
        </div>
        <div>
          <h3 className="text-lg font-bold">{post.title}</h3>
          <p>{post.body}</p>
        </div>
      </div>
    </li>
  )
}

export default ForumCard
