import Image from "next/image"

function AuthorImage({ id }: { id: number }) {
  return (
    <Image
      loading="eager"
      src={`https://randomuser.me/api/portraits/men/${id}.jpg`}
      width={32}
      height={32}
      alt={`avatar-image-${id}`}
      className="rounded-full"
    />
  )
}

export default AuthorImage
