import { TAuthor } from "@/service/post.service"
import AuthorImage from "./AuthorImage"

type TProps = {
  author?: TAuthor
}

function Author(props: TProps) {
  const { author } = props

  if (author == null) {
    return <span className="font-semibold">Unkown user</span>
  }

  return (
    <>
      <AuthorImage id={author.id} />
      <span className="text-orange-500 font-semibold">{author.name}</span>
    </>
  )
}

export default Author
