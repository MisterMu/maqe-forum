"use client"

import Image from "next/image"

const loader = ({ src, width }: { src: string; width: number }) => {
  const height = (width * 240) / 320
  return `https://picsum.photos/id/${src}/${width}/${height}`
}

function PostImage({ id }: { id: number }) {
  return (
    <Image
      loader={loader}
      loading="lazy"
      src={id.toString()}
      width={240}
      height={180}
      alt={`post-image-${id}`}
    />
  )
}

export default PostImage
