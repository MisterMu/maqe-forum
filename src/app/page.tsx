import Timezone from "./components/Timezone"
import ForumList from "./components/ForumList"

async function Home() {
  return (
    <div className="bg-gray-100 min-h-screen py-10 px-8">
      <div className=" max-w-screen-lg mx-auto">
        <h1 className="text-3xl font-bold mb-8">MAQE Forum</h1>
        <div className="mb-4">
          Your current timezone is: <Timezone />
        </div>
        <ForumList />
      </div>
    </div>
  )
}

export default Home
