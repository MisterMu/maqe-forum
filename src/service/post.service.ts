export type TPost = {
  id: number
  author_id: number
  title: string
  body: string
  image_url: string
  created_at: Date
}

export type TAuthor = {
  id: number
  name: string
  role: string
  place: string
  avatar_url: string
}

export type TPostWithAuthor = {
  id: number
  post: TPost
  author?: TAuthor
}

async function getPostWithAuthor(): Promise<TPostWithAuthor[]> {
  const responses = await Promise.all([
    fetch("https://maqe.github.io/json/authors.json"),
    fetch("https://maqe.github.io/json/posts.json"),
  ])
  if (responses.some((res) => !res.ok)) {
    throw new Error("Failed to fetch")
  }
  const authors: TAuthor[] = await responses[0].json()
  const posts: TPost[] = await responses[1].json()

  return posts.map((p) => {
    const author = authors.find((a) => a.id === p.author_id)
    return {
      id: p.id,
      post: p,
      author,
    }
  })
}

const postService = {
  getPostWithAuthor,
}

export default postService
